package com.example.tiendageli.view.ui.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tiendageli.view.ui.models.Comments
import com.example.tiendageli.view.ui.network.COMMENTS_COLLECTION_NAME
import com.google.firebase.firestore.FirebaseFirestore


private const val TAG = "CommentsDetailDialogViewModel"
class CommentsDetailDialogViewModel :ViewModel(){
    val firebaseFirestore = FirebaseFirestore.getInstance()
    private val _result = MutableLiveData<Exception?>()
    val result: LiveData<Exception?> get() = _result

    @SuppressLint("LongLogTag")
    fun postComment(comment: Comments){
        firebaseFirestore.collection(COMMENTS_COLLECTION_NAME)
            .add(comment)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshot added with ID: ${documentReference.id}")
                _result.value = null
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)
                _result.value = e
            }
    }
}