package com.example.tiendageli.view.ui.adapters

import com.example.tiendageli.view.ui.models.Comments

interface CommentsListener {
    fun OnCommentsClick(comment: Comments, position: Int)
}