package com.example.tiendageli.view.ui.viewmodel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tiendageli.view.ui.models.User
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

private const val TAG = "ProfileEditDialogViewModel"
class ProfileEditDialogViewModel: ViewModel() {
    val firebaseFirestore = FirebaseFirestore.getInstance()
    private val _result = MutableLiveData<Exception?>()
    val result: LiveData<Exception?> get() = _result
    val userInfo = Firebase.auth.currentUser

    @SuppressLint("LongLogTag")
    fun updateInfoUser(user: User){
        if (userInfo != null){
            firebaseFirestore.collection(USERS_COLLECTION_NAME).document(userInfo.uid)
                .set(user)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "DocumentSnapshot successfully written!")
                    _result.value = null
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "Error writing document", e)
                    _result.value = e
                }
        }
    }
}