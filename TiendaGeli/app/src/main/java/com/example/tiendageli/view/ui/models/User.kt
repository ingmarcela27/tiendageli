package com.example.tiendageli.view.ui.models

data class User(
    var displayName:String?=null,
    var address:String?=null,
    var phoneNumber:String?=null,
    var email:String?=null,
    var photoUrl:String?=null
)
