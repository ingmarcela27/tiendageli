package com.example.tiendageli.view.ui.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.net.Uri
import android.os.Bundle
import android.provider.SyncStateContract.Helpers.update
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tiendageli.R
import com.example.tiendageli.databinding.FragmentProfileEditDialogBinding
import com.example.tiendageli.view.ui.models.User
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.example.tiendageli.view.ui.viewmodel.CommentsDetailDialogViewModel
import com.example.tiendageli.view.ui.viewmodel.ProfileEditDialogViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Picasso

private const val TAG = "ProfileEdit"
class ProfileEditDialogFragment : BottomSheetDialogFragment(),View.OnClickListener {

    var imageUri: Uri? = null
    private var _binding:FragmentProfileEditDialogBinding? = null
    private val binding get() = _binding!!
    private val storageRef = FirebaseStorage.getInstance().reference
    private val db = Firebase.firestore

    private val user = Firebase.auth.currentUser!!
    var imgUrl: Uri? = null

    private lateinit var profileEditDialogViewModel: ProfileEditDialogViewModel

    private val startForProfileImageResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        result: ActivityResult ->

        val resultCode = result.resultCode
        val data = result.data

        if(resultCode == Activity.RESULT_OK) {
            val fileUri = data?.data
            imageUri = fileUri
            Picasso.get().load(imageUri).fit().centerCrop().into(binding.inPicture)
            uploadPicture()
        }else if(resultCode == ImagePicker.RESULT_ERROR){
            Toast.makeText(requireContext(), ImagePicker.getError(data),Toast.LENGTH_SHORT).show()
        }else {
            Toast.makeText(requireContext(), "Tarea Cancelada",Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        // Inflate the layout for this fragment
        _binding = FragmentProfileEditDialogBinding.inflate(inflater,container,false)
        var view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        profileEditDialogViewModel = ViewModelProvider(this).get(ProfileEditDialogViewModel::class.java)
        binding.floatAddImage.setOnClickListener(this)
        binding.btnSaveUpdate.setOnClickListener {

            val full_name = binding.inFullName.text.toString()
            val address = binding.inAddress.text.toString()
            val phone = binding.inPhone.text.toString()
            val email = binding.inEmail.text.toString()
            val usermodel = User()

            if (full_name.isEmpty()) {
                binding.inFullName.setError("Ingrese el nombre completo a modificar")
                binding.inFullName.requestFocus()

            }else if (address.isEmpty()) {
                binding.inAddress.setError("Ingrese la dirección a modificar")
                binding.inAddress.requestFocus()

            }else if (phone.isEmpty()) {
                binding.inPhone.setError("Ingrese su numero telefonico a modificar")
                binding.inPhone.requestFocus()


            }else if (email.isEmpty()) {
                binding.inEmail.setError("Ingrese su dirección de correo electronico a cambiar")
                binding.inEmail.requestFocus()
            }else {
                //UPDATE USER'S EMAIL
                user.updateEmail(email)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.d(TAG, "User email address updated.")
                            val message = "correo modificado con éxito"
                            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
                        }
                    }


                val photoUrl = imgUrl.toString()
                //update remaining fields
                val userRef = db.collection(USERS_COLLECTION_NAME).document(user.uid)
                userRef
                    .update("displayName", full_name, "address", address, "phoneNumber", phone, "email", email, "photoUrl", photoUrl)
                    .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully updated!")
                        dismiss() }
                    .addOnFailureListener { e -> Log.w(TAG, "Error updating document", e) }
            }
        }
    }

    override fun onClick(v: View?) {
        choosePicture()
    }

    private fun choosePicture() {
        ImagePicker.with(this)
            .crop()
            .maxResultSize(480, 480)
            .cropSquare()
            .createIntent { intent ->
                startForProfileImageResult.launch(intent)
            }
    }


    @SuppressLint("LongLogTag")
    fun uploadPicture(){
        //val randomKey = UUID.randomUUID().toString()

        if (user != null){
            // Create a reference to 'images/profile.jpg'
            val profileImagesRef = storageRef.child("images/" + user.uid)
            imageUri?.let {
                profileImagesRef.putFile(it).addOnFailureListener {

                }.addOnSuccessListener { taskSnapshot ->
                    Log.e(TAG, "Imagen almacenada correctamente")
                    val downloadURLTask = storageRef.child("images/" + user.uid)
                        .downloadUrl
                        .addOnSuccessListener {
                            val image_url = it
                            imgUrl = image_url
                        }
                }
            }
        }

    }
    /*
    fun observeViewModel() {

        profileEditDialogViewModel.result.observe(viewLifecycleOwner, Observer{
            val message = if (it == null){
                getString(R.string.edit_profile)
            }else {
                getString(R.string.error_update_profile)
            }

            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            dismiss()
        })
    }*/

}
