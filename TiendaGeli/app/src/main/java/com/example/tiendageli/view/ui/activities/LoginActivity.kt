package com.example.tiendageli.view.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import android.content.Intent
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.example.tiendageli.R
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

//import kotlinx.android.synthetic.main.activity_login.*


private const val TAG  = "LoginActivity"
class LoginActivity : AppCompatActivity() {
    // ADD VIEW VARIABLE DECLARATIONS HERE
    private lateinit var btnLogin: Button
    private lateinit var etEmail: EditText
    private lateinit var etPassword: EditText
    private lateinit var btnCreate: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        // Tiempo muerto de 3000 milisegundos
        Thread.sleep(3000)
        setTheme(R.style.Theme_TiendaGeli)
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        val auth = Firebase.auth
        if (auth.currentUser != null) {
            goMainActivity()
        }

        btnLogin = findViewById(R.id.btnLogin)
        etEmail = findViewById(R.id.etEmail)
        etPassword = findViewById(R.id.etPassword)
        btnCreate = findViewById(R.id.btnCreate)

        btnLogin.setOnClickListener{
            btnLogin.isEnabled = false
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()
            if (email.isBlank() || password.isBlank()){
                Toast.makeText(this, "El correo electrónico/contraseña no deben estar vacíos", Toast.LENGTH_SHORT).show()
                btnLogin.isEnabled = true
                return@setOnClickListener
            }
            // Firebase authentication check
            auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                btnLogin.isEnabled = true
                if (task.isSuccessful) {
                    Toast.makeText(this, "Inicio de Sesión Exitoso!", Toast.LENGTH_SHORT).show()
                    goMainActivity()
                } else {
                    Log.e(TAG, "Error al intentar ingresar", task.exception)
                    Toast.makeText(this, "Atenticación Fallida, Intentelo de nuevo", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnCreate.setOnClickListener{
            goRegisterActivity()
            /*btnCreate.isEnabled = false
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()
            if (email.isBlank() && password.isBlank()){
                Toast.makeText(this, "Email y/o contraseña se encuentran vacios", Toast.LENGTH_SHORT).show()
                btnCreate.isEnabled = true
                return@setOnClickListener
            }
            // Firebase create user
            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                btnCreate.isEnabled = true
                if (task.isSuccessful) {
                    Toast.makeText(this, "Se ha registrado de manera exitosa!", Toast.LENGTH_SHORT).show()
                    goMainActivity()
                } else {
                    Log.e(TAG, "La creación del usuario ha sido fallida, intentelo de nuevo", task.exception)
                    Toast.makeText(this, "Registro Fallido", Toast.LENGTH_SHORT).show()
                }
            }*/
        }

    }

    private fun goMainActivity() {
        Log.i(TAG, "goMainActivity")
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun goRegisterActivity(){
        Log.i(TAG, "goRegisterActivity")
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
    }
}