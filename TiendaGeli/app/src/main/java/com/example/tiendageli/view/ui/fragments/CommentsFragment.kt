package com.example.tiendageli.view.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.tiendageli.R
import com.example.tiendageli.databinding.FragmentCommentsBinding
import com.example.tiendageli.view.ui.adapters.CommentsAdapter
import com.example.tiendageli.view.ui.models.Comments
import com.example.tiendageli.view.ui.models.Products
import com.example.tiendageli.view.ui.viewmodel.CommentsViewModel
import com.example.tiendageli.view.ui.viewmodel.OrderViewModel

class CommentsFragment : Fragment() {

    private var _binding: FragmentCommentsBinding? = null
    private val binding get() = _binding!!

    private lateinit var commentsAdapter: CommentsAdapter
    private lateinit var commentsViewModel: CommentsViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentCommentsBinding.inflate(inflater,container,false)
        var view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentsViewModel = ViewModelProvider(this).get(CommentsViewModel::class.java)
        commentsViewModel.refresh()
        commentsAdapter = CommentsAdapter()

        binding.rvComments.apply {
            layoutManager = LinearLayoutManager(view.context,LinearLayoutManager.VERTICAL,false)
            adapter = commentsAdapter
        }

        binding.floatingAddComment.setOnClickListener{
            findNavController().navigate(R.id.commentsDetailFragmentDialog)
        }
        observeViewModel()
    }

    fun observeViewModel() {
        commentsViewModel.listComments.observe(viewLifecycleOwner, Observer<List<Comments>> {
                comments -> commentsAdapter.updateData(comments)
        })
        commentsViewModel.isLoading.observe(viewLifecycleOwner, Observer<Boolean>{
            if (it != null){
                binding.rlBaseComments.visibility = View.INVISIBLE
            }
        })
    }


}