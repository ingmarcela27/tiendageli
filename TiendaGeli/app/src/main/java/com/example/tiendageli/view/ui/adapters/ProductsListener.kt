package com.example.tiendageli.view.ui.adapters

import com.example.tiendageli.view.ui.models.Products

interface ProductsListener {
    fun OnProductsClick(product: Products, position: Int)
}