package com.example.tiendageli.view.ui.network

import com.example.tiendageli.view.ui.models.Comments
import com.example.tiendageli.view.ui.models.Products
import com.example.tiendageli.view.ui.models.User
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

const val PRODUCTS_COLLECTION_NAME = "products"
const val COMMENTS_COLLECTION_NAME = "comments"
const val USERS_COLLECTION_NAME = "users"
class FirestoreService {
    val firebaseFirestore = FirebaseFirestore.getInstance()
    val usersCollection = firebaseFirestore.collection(USERS_COLLECTION_NAME)
    val auth = Firebase.auth
    val userID = auth.uid

    fun getProducts(callback: Callback<List<Products>>){
        firebaseFirestore.collection(PRODUCTS_COLLECTION_NAME)
            .get()
            .addOnSuccessListener { result ->
                for (doc in result) {
                    val list = result.toObjects(Products::class.java)
                    callback.onSuccess(list)
                    break
                }
            }
    }

    fun getComments(callback: Callback<List<Comments>>){
        firebaseFirestore.collection(COMMENTS_COLLECTION_NAME)
            .get()
            .addOnSuccessListener { result ->
                for (doc in result) {
                    val list = result.toObjects(Comments::class.java)
                    callback.onSuccess(list)
                    break
                }
            }
    }

    fun addUser(user: User?) {
        user?.let {
            GlobalScope.launch(Dispatchers.IO) {
                usersCollection.document(userID.toString()).set(it)
            }
        }
    }


}