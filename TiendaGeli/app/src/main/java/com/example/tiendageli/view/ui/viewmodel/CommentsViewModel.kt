package com.example.tiendageli.view.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tiendageli.view.ui.models.Comments
import com.example.tiendageli.view.ui.network.Callback
import com.example.tiendageli.view.ui.network.FirestoreService
import java.lang.Exception

class CommentsViewModel: ViewModel() {
    val firestoreService = FirestoreService()
    var listComments : MutableLiveData<List<Comments>> = MutableLiveData()
    var isLoading = MutableLiveData<Boolean>()

    fun refresh(){
        getCommentsFromFirebase()
    }

    fun getCommentsFromFirebase(){
        firestoreService.getComments(object : Callback<List<Comments>>{
            override fun onSuccess(result: List<Comments>?) {
                listComments.postValue(result)
                processFinished()
            }

            override fun onFailed(exception: Exception) {
                processFinished()
            }
        })
    }

    private fun processFinished() {
        isLoading.value = true
    }
}