package com.example.tiendageli.view.ui.models

import java.io.Serializable

class Products: Serializable {
    lateinit var name: String
    lateinit var short_desc: String
    lateinit var long_desc: String
    lateinit var price: String
    lateinit var image: String
}