package com.example.tiendageli.view.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.tiendageli.R
import com.example.tiendageli.databinding.FragmentProfileBinding
import com.example.tiendageli.view.ui.activities.LoginActivity
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await


private const val TAG ="ProfileFragment"
class ProfileFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var btnLogout: Button

    private var _binding:FragmentProfileBinding? = null
    private val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        var view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLogout.setOnClickListener {
            Log.i("ProfileFragment", "El usuario quiere cerrar la sesión")
            Firebase.auth.signOut()
            val intent = Intent(activity, LoginActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }
        binding.btnEditProfile.setOnClickListener {
            //val actionUpdateProfile = ProfileFragmentDirections.actionUpdateProfile()
            //Navigation.findNavController(it).navigate(actionUpdateProfile)
            findNavController().navigate(R.id.profileEditFragmentDialog)
        }
        getProfileData()
    }

    private fun getProfileData() = CoroutineScope(Dispatchers.IO).launch {
        try{
            val user = Firebase.auth.currentUser
            if(user != null){
                val firebaseFirestore = Firebase.firestore.collection(USERS_COLLECTION_NAME).document(user.uid)
                val profile = firebaseFirestore.get().await()
                delay(1000L)
                withContext(Dispatchers.Main) {
                    binding.pfFullname.setText(profile.data?.get("displayName").toString())
                    binding.pfEmail.setText(profile.data?.get("email").toString())
                    binding.pfAddress.setText(profile.data?.get("address").toString())
                    binding.pfPhone.setText(profile.data?.get("phoneNumber").toString())
                    Picasso.get().load(profile.data?.get("photoUrl").toString()).fit().centerCrop().into(binding.pfImage)
                }

            }

        }catch (e: Exception) {

        }

    }


}