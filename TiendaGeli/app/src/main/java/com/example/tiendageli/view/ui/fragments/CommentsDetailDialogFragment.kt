package com.example.tiendageli.view.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tiendageli.R
import com.example.tiendageli.databinding.FragmentCommentsDetailDialogBinding
import com.example.tiendageli.view.ui.models.Comments
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.example.tiendageli.view.ui.viewmodel.CommentsDetailDialogViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase


class CommentsDetailDialogFragment : BottomSheetDialogFragment() {

    private var _binding: FragmentCommentsDetailDialogBinding? = null
    private val binding get() = _binding!!

    private lateinit var commentsDetailViewModel: CommentsDetailDialogViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getDialog()?.getWindow()?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        // Inflate the layout for this fragment
        _binding = FragmentCommentsDetailDialogBinding.inflate(inflater,container,false)
        var view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        commentsDetailViewModel = ViewModelProvider(this).get(CommentsDetailDialogViewModel::class.java)
        binding.btnSendComment.setOnClickListener {
            setCommentUser()
        }
        observeViewModel()
    }

    fun observeViewModel() {

        commentsDetailViewModel.result.observe(viewLifecycleOwner, Observer{
            val message = if (it == null){
                getString(R.string.add_comment)
            }else {
                getString(R.string.error_comment)
            }

            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
            dismiss()
        })
    }

    private fun setCommentUser(){
        val user = Firebase.auth.currentUser
        val bodyComment = binding.txtComment.text.toString().trim()
        val score = binding.stars.rating.toString()

        if (bodyComment.isEmpty()){
            binding.txtComment.error = "El comentario no puede estar vacio"
            binding.txtComment.requestFocus()
        }
        if(user != null){
            val comment = Comments()
            val firebaseFirestore = FirebaseFirestore.getInstance()
            firebaseFirestore.collection(USERS_COLLECTION_NAME).document(user.uid)
                .get()
                .addOnSuccessListener { document ->
                    if (document.exists()){
                        val nombreUsuario = document.data?.get("displayName").toString()
                        val imgUsuario = document.data?.get("photoUrl").toString()
                        comment.name_user = nombreUsuario
                        comment.description = bodyComment
                        comment.rating = score
                        comment.icon_profile = imgUsuario

                        commentsDetailViewModel.postComment(comment)
                    } else{
                        Log.i("prueba", "error a la hora de hallar el documento")
                    }
                }


        }
    }

}