package com.example.tiendageli.view.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.tiendageli.R
import com.example.tiendageli.view.ui.models.User
import com.example.tiendageli.view.ui.network.FirestoreService
import com.example.tiendageli.view.ui.network.USERS_COLLECTION_NAME
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

private const val TAG = "RegisterActivity"

class RegisterActivity : AppCompatActivity() {

    private lateinit var txt_name: EditText
    private lateinit var txt_address: EditText
    private lateinit var txt_phone: EditText
    private lateinit var txt_email: EditText
    private lateinit var txt_clave: EditText
    private lateinit var btn_saveUser: Button
    private lateinit var btn_dirlogin: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        txt_name = findViewById(R.id.txtFullName)
        txt_address = findViewById(R.id.txtAddress)
        txt_phone = findViewById(R.id.txtPhone)
        txt_email = findViewById(R.id.txtEmail)
        txt_clave = findViewById(R.id.txtClave)
        btn_saveUser = findViewById(R.id.btnCreate)
        btn_dirlogin = findViewById(R.id.btnlog)

        btn_saveUser.setOnClickListener {
            createUser()
        }

        btn_dirlogin.setOnClickListener {
            goLoginActivity()
        }

    }

    private fun createUser(){
        val auth = Firebase.auth
        val firebaseFirestore = FirebaseFirestore.getInstance()

        val full_name = txt_name.text.toString()
        val address = txt_address.text.toString()
        val phone = txt_phone.text.toString()
        val email = txt_email.text.toString()
        val password = txt_clave.text.toString()

        val user = User()
        user.displayName = full_name
        user.address = address
        user.phoneNumber = phone
        user.email = email

        if(full_name.isEmpty()){
            txt_name.setError("Ingrese su nombre completo")
            txt_name.requestFocus()

        }else if(address.isEmpty()){
            txt_address.setError("Ingrese un dirección valida")
            txt_address.requestFocus()

        }else if(phone.isEmpty()){
            txt_phone.setError("Ingrese su numero telefonico")
            txt_phone.requestFocus()

        }else if(email.isEmpty()){
            txt_email.setError("Ingrese su dirección de correo electronico")
            txt_phone.requestFocus()
        }else if (password.isEmpty()){
            txt_clave.setError("Ingrese una contraseña valida")
            txt_phone.requestFocus()

        }else {

            auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener { task ->
                if (task.isSuccessful) {

                    val firestore = FirestoreService()
                    firestore.addUser(user)
                    Toast.makeText(this, "Se ha registrado de manera exitosa!", Toast.LENGTH_SHORT).show()
                    goLoginActivity()

                } else {
                    Log.e(TAG, "La creación del usuario ha sido fallida, intentelo de nuevo", task.exception)
                    Toast.makeText(this, "Registro Fallido", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }

    private fun goLoginActivity() {
        Log.i(TAG, "goLoginActivity")
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }


}