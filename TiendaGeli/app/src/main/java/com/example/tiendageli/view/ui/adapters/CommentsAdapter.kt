package com.example.tiendageli.view.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tiendageli.R
import com.example.tiendageli.view.ui.models.Comments
import com.squareup.picasso.Picasso

class CommentsAdapter : RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {
    var listComments = ArrayList<Comments>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(
        R.layout.item_comments,parent,false))

    override fun onBindViewHolder(holder: CommentsAdapter.ViewHolder, position: Int) {
        val comments = listComments[position]
        holder.user_name_comment.text = comments.name_user
        holder.comment.text = comments.description
        holder.rating.text = comments.rating
        Picasso.get().load(comments.icon_profile).into(holder.img_user)
    }

    override fun getItemCount() = listComments.size

    fun updateData(data: List<Comments>){
        listComments.clear()
        listComments.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val user_name_comment = itemView.findViewById<TextView>(R.id.usernameComment)
        val comment = itemView.findViewById<TextView>(R.id.descriptionComment)
        val rating = itemView.findViewById<TextView>(R.id.scoreComment)
        val img_user = itemView.findViewById<ImageView>(R.id.iconProfileComment)
    }
}