package com.example.tiendageli.view.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tiendageli.R
import com.example.tiendageli.view.ui.models.Products
import com.squareup.picasso.Picasso

class ProductsAdapter(val productsListener: ProductsListener) : RecyclerView.Adapter<ProductsAdapter.ViewHolder> () {

    var listProducts = ArrayList<Products>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_product,parent,false))


    override fun onBindViewHolder(holder: ProductsAdapter.ViewHolder, position: Int) {
        val products = listProducts[position]
        holder.name_product.text = products.name
        holder.description_product.text = products.short_desc
        holder.price_product.text = products.price
        Picasso.get().load(products.image).into(holder.picture_product)

        holder.itemView.setOnClickListener{
            productsListener.OnProductsClick(products, position)
        }
    }

    override fun getItemCount()=listProducts.size

    fun updateData(data: List<Products>){
        listProducts.clear()
        listProducts.addAll(data)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val name_product = itemView.findViewById<TextView>(R.id.title_product)
        val description_product = itemView.findViewById<TextView>(R.id.short_descProduct)
        val price_product = itemView.findViewById<TextView>(R.id.price_product)
        val picture_product = itemView.findViewById<ImageView>(R.id.iconProduct)
    }
}