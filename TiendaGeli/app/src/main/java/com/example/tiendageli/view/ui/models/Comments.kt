package com.example.tiendageli.view.ui.models

data class Comments(
    var icon_profile:String?=null,
    var name_user:String?=null,
    var description:String?=null,
    var rating:String?=null
)
